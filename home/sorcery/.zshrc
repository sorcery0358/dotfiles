HISTFILE=~/.zshistory
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory

source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

#alias mv='mv -i'
#alias rm='rm -i'

bindkey  "^[[H"   beginning-of-line
bindkey  "^[[4~"   end-of-line
bindkey  "^[[P"  delete-char

export HISTCONTROL=ignoreboth

colorscript -e suckless
./.repos/st-sorcery/st-0.8.4/fetch-master-6000/fm6000 -f ".repos/st-sorcery/st-0.8.4/fetch-master-6000/arch-small-art" -c "bright_white" -n -de "dwm" -l "16" -o "arch linux" -s "zsh / starship" -g "5"

eval "$(starship init zsh)"
