#!/bin/sh

DUAL_FILE=/tmp/dual-screen

if [ -e $DUAL_FILE ]; then
	rm $DUAL_FILE
	xrandr --output eDP1 --primary --mode 1920x1080 --pos 1920x0 --rotate normal --output HDMI-1-0 --off &
	sleep 3
	pactl set-default-sink alsa_output.pci-0000_00_1f.3.analog-stereo &
	pulseaudio-equalizer enable
else
	touch $DUAL_FILE
	xrandr --output eDP1 --mode 1920x1080 --pos 1920x0 --rotate normal --output HDMI-1-0 --primary --mode 1920x1080 --pos 0x0 --rotate normal &
	sleep 3
	pactl set-default-sink alsa_output.pci-0000_01_00.1.hdmi-stereo &
	pulseaudio-equalizer enable
fi