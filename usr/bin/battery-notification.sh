#!/bin/sh

CHARGE_FILE=/tmp/battery-charging
DISCHARGE_FILE=//tmp/battery-discharging
LOW_FILE=/tmp/battery-low

while true
do

	CHARGE=$(cat /sys/class/power_supply/BAT0/capacity)
	STATUS=$(cat /sys/class/power_supply/BAT0/status)

	if [ "$STATUS" = "Charging" ] || [ "$STATUS" = "Not charging" ]; then
		if [ -e $LOW_FILE ]; then
			rm $LOW_FILE
		fi
		if [ -e $CHARGE_FILE ]; then
			:
		else
			if [ -e $DISCHARGE_FILE ]; then
				rm $DISCHARGE_FILE
			fi
			touch $CHARGE_FILE
			if [ "$CHARGE" -gt 0 ] && [ "$CHARGE" -le 15 ]; then
				notify-send "Charging. 󰢟 $CHARGE%"
			elif [ "$CHARGE" -gt 15 ] && [ "$CHARGE" -le 50 ]; then
				notify-send "Charging. 󱊤 $CHARGE%"
			elif [ "$CHARGE" -gt 50 ] && [ "$CHARGE" -le 75 ]; then
				notify-send "Charging. 󱊥 $CHARGE%"
			else
				notify-send "Charging. 󱊦 $CHARGE%"
			fi
		fi

	else
		if [ -e $DISCHARGE_FILE ]; then
			if [ "$CHARGE" == 5 ] || [ "$CHARGE" == 10 ] || [ "$CHARGE" == 15 ]; then
				if [ -e $LOW_FILE ]; then
					:
				else
					touch $LOW_FILE
					notify-send "Battery low. 󱃍 $CHARGE%"
				fi
			else
				if [ -e $LOW_FILE ]; then
					rm $LOW_FILE
				fi
			fi
		else
			if [ -e $CHARGE_FILE ]; then
				rm $CHARGE_FILE
			fi
			touch $DISCHARGE_FILE
			if [ "$CHARGE" -gt 0 ] && [ "$CHARGE" -le 15 ]; then
				notify-send "Discharging. 󱃍 $CHARGE%"
			elif [ "$CHARGE" -gt 15 ] && [ "$CHARGE" -le 50 ]; then
				notify-send "Discharging. 󱊡 $CHARGE%"
			elif [ "$CHARGE" -gt 50 ] && [ "$CHARGE" -le 75 ]; then
				notify-send "Discharging. 󱊢 $CHARGE%"
			else
				notify-send "Discharging. 󱊣 $CHARGE%"
			fi
		fi
	fi

	sleep 1
done
