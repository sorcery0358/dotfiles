#!/bin/sh

if ps -C spotify > /dev/null; then
	playerctl --player=spotify play-pause
else
	spotify
fi