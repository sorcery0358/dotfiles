#!/bin/sh

DISABLED=$(xinput list-props "TPPS/2 Elan TrackPoint" | grep "Device Enabled" | awk '{print $4}')

if [ "$DISABLED" == 1 ]; then
	xinput disable "TPPS/2 Elan TrackPoint"
else
	xinput enable "TPPS/2 Elan TrackPoint"
fi