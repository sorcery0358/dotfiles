#!/bin/sh

sinks=$(pamixer --list-sinks | grep ^[0-9] | awk '{print $1}' | xargs)

for i in $sinks; do
    pamixer --sink $i --toggle-mute ;
done